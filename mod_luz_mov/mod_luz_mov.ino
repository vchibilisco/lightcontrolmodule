//Declaración librerias
#include <SPI.h>
#include <Ethernet.h>

//Variables de configuracion
#define STATUS = "STATUS";
#define LIGHTSTATE = "LIGHTCONTROL";
#define MSLIGHT = "MSLIGHT";
#define LSLIGHT = "LSLIGHT";


//Variables de activacion
boolean light, mslight, lslight;

//Configuracion de Shield Ethernet Mac e IP 
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,177);

//Configuración puerto de servidor
EthernetServer server(80);

//Variables para Shield Ethernet
String readString=String(30);
String state="";
int action;
String device = "X515SXS5X11XS51A2X3";

//Pin Rele, Utiliza valores inversos
// LOW => HIGH
// HIGH => LOW
int pinRele = 5;

//Variables de Sensor Pir
int sensorPir = 2;               // Pin digital del sensor PIR
int movimiento = LOW;           // Variable sincroniza escritura en puerto Serial
int valorSensorPir = 0;                    // variable para leer el estado del pin

int calibrationTime = 60;       // Esto depende de cada sensor

//Auxiliares para calibrar el tiempo que considera que hay movimiento
long time = 10000, timeaux1 = 0, timeaux2 = 0;

void setup() {
  delay(10000);
  Serial.begin(9600);
  
  //Configura puerto de sensor pir
  pinMode(sensorPir, INPUT);     // declare sensor as input  
  
  Serial.print("Calibrando Sensor Pir ...");
  for (int i = 0; i < calibrationTime; i++) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("Sensor Pir Calibrado");
  delay(50);
  
  //Configura interrupcion 0 para sensor pir 
  //attachInterrupt(0,MotionDetection,CHANGE);
  
  //Inicia Ehternet con Mac e IP
  Ethernet.begin(mac, ip);
  //Inicia server
  server.begin();
  Serial.print("Servidor iniciado");
  Serial.println("IP fija configurada");
  Serial.println(Ethernet.localIP());
  
  //Configuracion de pin para rele 
  pinMode(pinRele,OUTPUT);
  DesactivateRele();
  
  SetMslight(true);
  
  Serial.println("Ready");
}

void loop() {
  //noInterrupts();
  EthernetClient client= server.available();
  if(client)
  {
    CustomerAvailable(client);
  }
  delay(1000);
  //interrupts();
 
  MotionDetection();
}

//Interrupcion por movimiento
void MotionDetection(){
  if(mslight){
    valorSensorPir = digitalRead(sensorPir);   // Leemos el Sensor Pir  
    if (valorSensorPir == HIGH) {             // Si lo hay
      if (movimiento == LOW) {     //Fijarse que, de no existir "movimiento", cambiamos estado de pinRele   
        timeaux1 = millis();
        ActiveRele();  // activo rele
        //Registramos el cambio de estado
        movimiento = HIGH;      
      }
    } else {
      if (movimiento == HIGH) {   // Fijarse que, de no existir "movimiento", cambiamos estado de pinRele
        timeaux2 = millis();
        long aux = timeaux2 - timeaux1;      
        if (aux > time ) {
          DesactivateRele(); // desactivo rele
          //Registramos el cambio de estado
          movimiento = LOW;        
        }
      }
    }
  }
}

void CustomerAvailable(EthernetClient client){
  boolean lineaenblanco=true;
    while(client.connected())//client conectado
    {
      Serial.println("new client");
      if(client.available())
      {
        char c=client.read();
        if(readString.length()<30)//Leemos petición HTTP caracter a caracter
        {        
          readString.concat(c); //Almacenar los caracteres en la variable readString         
        }
        
        if(c=='\n' && lineaenblanco)//Si la petición HTTP ha finalizado
        {
          action = readString.indexOf("LIGHTCONTROL=");
          if(action > 0){
            LightControlAction(readString); 
            GetLightControl(client);           
          }else{
            action = readString.indexOf("MSLIGHT=");
            if(action > 0){     
              ActionMotionSensor(readString);
              GetMotionSensor(client);
            }else{
              action = readString.indexOf("STATUS=");
              if(action > 0){
                GetStatus(client);
              }
            }         
          }
          client.stop();//Cierro conexión con el cliente
          readString="";
          Serial.println("client disconnected");       
        }
      }
    }
}

void GetMotionSensor(EthernetClient client){
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/json");    
  client.println();  
  client.print("{\"device\":\""+String(device)+"\",\"status\":[{\"mslight\":\""+ String(mslight) +"\"\}]}");
  delay(1);
}

void GetLightControl(EthernetClient client){
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/json");    
  client.println();  
  client.print("{\"device\":\""+String(device)+"\",\"status\":[{\"lightcontrol\":\""+ String(light) +"\"}]}");
  delay(1);
}

void GetStatus(EthernetClient client){
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/json");    
  client.println();  
  client.print("{\"device\":\""+String(device)+"\",\"status\":[{\"mslight\":\""+ String(mslight) +"\"\}, {\"lightcontrol\":\""+ String(light) +"\"}]}");
  delay(1);
}

void ActionMotionSensor(String action){
  Serial.println("MSLIGHT");
  Serial.println(action);
  if(action.indexOf("TRUE") > 0)
  {
    SetMslight(true);
    SetState("ON");
  } else if (action.indexOf("FALSE") > 0)
  {
    SetState("OFF");
    SetMslight(false);
  }
}

void LightControlAction(String action){
  Serial.println("LIGHTCONTROL");
  Serial.println(action);
  if(action.indexOf("TRUE") > 0)
  {
    ActiveRele();
    SetState("ON");
    SetLight(true);
  } else if (action.indexOf("FALSE") > 0)
  {
    SetLight(false);
    SetState("OFF");
    DesactivateRele();
  }
  
}

void ActiveRele(){
  digitalWrite(pinRele,LOW);
}

void DesactivateRele(){
  digitalWrite(pinRele,HIGH);
}

void SetState(String value){
  state = value;
}

void SetMslight(boolean value){
  mslight = value;
}

void SetLight(boolean value){
  light = value;
}
